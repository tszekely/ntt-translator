import { createTypes, actionCreator } from "redux-action-creator";

export const TRANSLATIONS_ACTION_TYPES = createTypes(["ADD"], "TRANSLATIONS");

export const addTranslation = actionCreator(
    TRANSLATIONS_ACTION_TYPES.ADD, 
    "addTranslationFromText",
    "addTranslationFromLang",
    "addTranslationToText",
    "addTranslationToLang"
)