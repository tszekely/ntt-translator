import { createTypes, actionCreator } from "redux-action-creator";

export const UI_ACTION_TYPES = createTypes(["UPDATE_FIELD"], "UI");

export const updateField = actionCreator(UI_ACTION_TYPES.UPDATE_FIELD, "field", "value")