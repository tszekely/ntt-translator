import React, { Component } from "react";
import { connect } from "react-redux";
import { path, prop, identity } from "ramda";
import TranslationBox from "../../components/TranslationBox";
import {addTranslation} from "../../actions/translationsActions";
import { updateField } from "../../actions/uiActions";

class TranslationAdd extends Component {
  addTranslation = () => this.props.addTranslation(this.props.ui)

  updateField = field => targetValue => this.props.updateField({
    field,
    value: path(["target", "value"], targetValue)
  })

  render() {
    const ui = this.props.ui;

    return (
      <TranslationBox
        title="Add Translation"
        isEditable={true}
        leftLangSelectChangeHandler={
          this.updateField("addTranslationFromLang")
        }
        leftLangSelectValue={
          prop("addTranslationFromLang", ui)
        }
        leftTextInputChangeHandler={
          this.updateField("addTranslationFromText")
        }
        leftTextInputValue={
          prop("addTranslationFromText", ui)
        }
        rightLangSelectChangeHandler={
          this.updateField("addTranslationToLang")
        }      
        rightLangSelectValue={
          prop("addTranslationToLang", ui)
        } 
        rightTextInputChangeHandler={
          this.updateField("addTranslationToText")
        }
        rightTextInputValue={
          prop("addTranslationToText", ui)
        }
        buttonClickHandler={this.addTranslation} />
    );
  }
}

export default connect(
  identity, {
  addTranslation,
  updateField
})(TranslationAdd);
