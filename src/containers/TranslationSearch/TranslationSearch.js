import React, { Component } from "react";
import { connect } from "react-redux";
import { path, prop, assocPath, find, propEq, defaultTo, compose } from "ramda";
import TranslationBox from "../../components/TranslationBox";
import { updateField } from "../../actions/uiActions";

class TranslationSearch extends Component {
  updateField = field => targetValue => this.props.updateField({
    field,
    value: path(["target", "value"], targetValue)
  })

  render() {
    const ui = this.props.ui;

    return (
      <TranslationBox
        title="Find translation"
        leftLangSelectChangeHandler={
          this.updateField("getTranslationFromLang")
        }
        leftLangSelectValue={
          prop("getTranslationFromLang", ui)
        }
        leftTextInputChangeHandler={
          this.updateField("getTranslationFromText")
        }
        leftTextInputValue={
          prop("getTranslationFromText", ui)
        }
        rightLangSelectChangeHandler={
          this.updateField("getTranslationToLang")
        }      
        rightLangSelectValue={
          prop("getTranslationToLang", ui)
        } 
        rightTextInputValue={
          prop("getTranslationToText", ui)
        } />
    );
  }
}

export default connect(
  state => assocPath(
    ["ui", "getTranslationToText"],
    !!path(["ui", "getTranslationFromText"], state) ?
      compose(
        defaultTo("Not found"),
        prop(path(["ui", "getTranslationToLang"], state)),
        find(
          propEq(
            path(["ui", "getTranslationFromLang"], state),
            path(["ui", "getTranslationFromText"], state)
          )
        )
      )(state.translations) :
      "",
    state
  ), {
    updateField
  })(TranslationSearch);
