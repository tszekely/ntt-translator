import { findIndex, propEq, adjust, append, assoc } from "ramda";
import { TRANSLATIONS_ACTION_TYPES } from "../actions/translationsActions";

const translations = (state = [], { type, payload }) => {
    switch (type) {
        case TRANSLATIONS_ACTION_TYPES.ADD:
            const i = findIndex(
                propEq(payload.addTranslationFromLang, payload.addTranslationFromText), 
                state
            )

            return i >= 0 ? 
                adjust(
                    assoc(payload.addTranslationToLang, payload.addTranslationToText),
                    i,
                    state
                ) :
                append({
                    [payload.addTranslationFromLang]: payload.addTranslationFromText,
                    [payload.addTranslationToLang]: payload.addTranslationToText,
                }, state);
        default:
            return state;
    }
};

export default translations;