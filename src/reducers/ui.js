import { assoc } from "ramda";
import { UI_ACTION_TYPES } from "../actions/uiActions";
import { TRANSLATIONS_ACTION_TYPES } from "../actions/translationsActions";

const ui = (state = {
    addTranslationFromText: "",
    addTranslationFromLang: "en",
    addTranslationToText: "",
    addTranslationToLang: "de",
    getTranslationFromText: "",
    getTranslationFromLang: "en",
    getTranslationToLang: "de",
}, {payload, type}) => {
    switch (type) {
        case UI_ACTION_TYPES.UPDATE_FIELD:
            return assoc(
                payload.field,
                payload.value,
                state
            );
        case TRANSLATIONS_ACTION_TYPES.ADD:
            return {
                ...state,
                addTranslationFromText: "",
                addTranslationToText: "",
            }
        default:
            return state;
    }
};

export default ui;