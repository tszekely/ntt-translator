import { combineReducers } from "redux"
import translations from "./translations"
import ui from "./ui"

export default combineReducers({
    translations,
    ui
})