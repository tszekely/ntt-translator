import React from "react";
import PropTypes from "proptypes";
import InputText from "../InputText";
import SelectLanguage from "../SelectLanguage";
import Button from "../Button";
import "./TranslationBox.css";

const TranslationBox = ({
    leftLangSelectChangeHandler,
    leftLangSelectValue,
    leftTextInputChangeHandler,
    leftTextInputValue,
    rightLangSelectChangeHandler,
    rightLangSelectValue,
    rightTextInputChangeHandler,
    rightTextInputValue,
    isEditable,
    buttonClickHandler,
    title
}) => {
    return (
        <section className="TranslationBox">
          <h3 className="TranslationBox-header">{title}</h3>
          <form className="TranslationBox-body">
            <fieldset>
              <SelectLanguage 
                onChange={leftLangSelectChangeHandler} 
                value={leftLangSelectValue} />
              <InputText 
                placeholder="Type a word..." 
                onChange={leftTextInputChangeHandler}
                value={leftTextInputValue} />
            </fieldset>

            <span className="TranslationBox-separator">➔</span>
    
            <fieldset>
              <SelectLanguage
                onChange={rightLangSelectChangeHandler} 
                value={rightLangSelectValue} />
                {
                   isEditable ? (
                    <InputText 
                      placeholder="Type a translation..."
                      onChange={rightTextInputChangeHandler}
                      value={rightTextInputValue} />
                   ) : (
                    <InputText
                      readOnly
                      defaultValue={rightTextInputValue} />
                   )
                }        
            </fieldset>
          </form>

          {
              isEditable && (
                <div className="TranslationBox-footer">
                    <Button 
                      disabled={
                        !leftTextInputValue || 
                        !rightTextInputValue || 
                        (leftLangSelectValue === rightLangSelectValue)
                      }
                      onClick={buttonClickHandler}>
                      Save translation
                    </Button>
                </div>
              )
          }
        </section>
    )
};

TranslationBox.propTypes = {
  leftLangSelectChangeHandler: PropTypes.func.isRequired,
  leftLangSelectValue: PropTypes.string,
  leftTextInputChangeHandler: PropTypes.func.isRequired,
  leftTextInputValue: PropTypes.string,
  rightLangSelectChangeHandler: PropTypes.func.isRequired,
  rightLangSelectValue: PropTypes.string,
  rightTextInputChangeHandler: PropTypes.func,
  rightTextInputValue: PropTypes.string,
  buttonClickHandler: PropTypes.func,
  isEditable: PropTypes.bool,
  title: PropTypes.string.isRequired
}

export default TranslationBox;