import React from 'react';
import cx from "classnames";
import "./InputText.css";

const InputText = (props) => {
  const classNames = cx({
    "InputText": true,
  });

  return (
    <input type="text" className={classNames} {...props} />
  );
}

export default InputText;
