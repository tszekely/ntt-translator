import React, { Component } from 'react';
import './App.css';
import TranslationSearch from "../../containers/TranslationSearch"
import TranslationAdd from "../../containers/TranslationAdd"

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">NTT-Translator</h1>
        </header>
        <main className="App-body">
          <TranslationSearch />
          <TranslationAdd />
        </main>
      </div>
    );
  }
}

export default App;
