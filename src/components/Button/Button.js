import React from "react";
import "./Button.css";

const Button = ({onClick, children = null, disabled = false}) => {
  return (
    <button className="Button" disabled={disabled} onClick={onClick}>{children}</button>
  );
}

export default Button;
