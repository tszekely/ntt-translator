import React from 'react';
import "./SelectLanguage.css";
import LANGUAGES from "../../constants/languageCodes.json"

const SelectLanguage = ({onChange, value}) => {
  return (
    <select className="SelectLanguage" value={value} onChange={onChange}>
      {
        LANGUAGES.map(l => (<option key={l.alpha2} value={l.alpha2}>{l.English}</option>))
      }
    </select>
  );
}

export default SelectLanguage;
